<?php
 Yii::app()->clientScript->registerCoreScript('jquery');
/* @var $this PessoaController */
/* @var $model Pessoa */
/* @var $form CActiveForm */

//direcao relacionado a visualização na criacao
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pessoa-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nome'); ?>
		<?php echo $form->textField($model,'nome',array('size'=>60,'maxlength'=>100)); ?>		

		<?php echo $form->error($model,'nome'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cpf'); ?>
		<?php echo $form->textField($model,'cpf',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'cpf'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($modelEstado,'id'); ?>
		
		<?php 
			
		/*	echo $form->dropDownList($modelEstado, 'id', CHtml::listData(Estado::model()->findAll(), 'id', 'nome' ),
				array(
                    'prompt'=>'Selecione',
                    'ajax' => array(
                        'type'=>'POST',
                        'url'=>CController::createUrl('Cidade/consultaCidade'), 
                        'dataType'=>'json',
                        'data'=>array('id_estado'=>'js:this.value'),
                        'success'=>'function(data) {
                            $("#Pessoa_id_cidade").html(data);                            
                        }',
                        'error' => 'function(){
                        	alert("Não teve sucesso");
                        }',
                )));

*/
		?>

		<?php 
			
			echo $form->dropDownList($modelEstado, 'id', CHtml::listData(Estado::model()->findAll(), 'id', 'nome' ),
				array());

		?>

		<?php echo $form->error($modelEstado,'id'); ?>
	</div>

	<!--carregando dados cidade do banco no drop-->
	<div class="row">
		<?php echo $form->labelEx($model,'id_cidade'); ?>
		<?php 
			//echo $form->dropDownList($model, 'id_cidade', CHtml::listData(array(),'id','nome')); 
			 echo $form->dropDownList($model,'id_cidade', array(), array('prompt'=>'Selecione'));
		?>
		<?php echo $form->error($model,'id_cidade'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'observacao'); ?>
		<?php echo $form->textArea($model,'observacao',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'observacao'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'senha'); ?>
		<?php echo $form->textField($model,'senha'); ?>
		<?php echo $form->error($model,'senha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario'); ?>
		<?php echo $form->textField($model,'usuario',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'usuario'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php

		Yii::app()->clientScript->registerScript("search", "
			$('#Estado_id').change(function() {
				var p = {
					id_estado: $(this).val()
				};
			    $.ajax({
	                type: 'POST',
	                data: p,
	                url: '".CController::createUrl('Cidade/consultaCidade')."',
	                success: function(data){
	                	var result = JSON.parse(data);
	                	comboCidade = '<option value=\'\'>Selecione</option>';
	                 	$.each(result,function(id,value) 
		                {
	                		comboCidade += '<option value='+id+'>'+value+'</option>';
		                });

	                    $('#Pessoa_id_cidade').html(comboCidade); 
	                },
	                error: function(data){
	                	alert('Ocorreu um erro ao consultar via ajax');
	                }
	           	});
		     });
		");


				
/*
		$('#id_estado').change(function() {
		    var val = $(this).val();
		    var id_cidade = $('#id_cidade');
		    var cm_desc_input_elem = $('#cm_desc_input');
			    if(val === COMPARE_WITH_YOUR_VALUE) {
			        $(cm_desc_select_elem).show();
			    } else {
			        $(cm_desc_input_elem).show();

		    }
 		});*/

 ?>